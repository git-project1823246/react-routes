import React from 'react';
import styled from 'styled-components';
import Logo from '../Logo/Logo';
import Button from '../Button/Button';

const HeaderContainer = styled.header`
display: flex;
justify-content: space-between;
align-items: center;
padding: 10px 20px;
background-color: #ffffff;
border-bottom: 1px solid #e9ecef;
`;

const LogoContainer = styled.div`
display: flex;
align-items: center;
`;

const LogoText = styled.span`
margin-left: 10px;
font-size: 20px;
font-weight: bold;
color: #333;
`;

const UserContainer = styled.div`
display: flex;
align-items: center;
`;

const UserName = styled.span`
margin-right: 10px;
font-size: 16px;
color: #333;
`;

const Header = () => {
return (
  <HeaderContainer>
    <LogoContainer>
      <Logo />
    </LogoContainer>
    <UserContainer>
      <UserName>Harry Potter</UserName>
      <Button buttonText="LOGIN" onClick={() => {}} />
    </UserContainer>
  </HeaderContainer>
);
};

export default Header;
