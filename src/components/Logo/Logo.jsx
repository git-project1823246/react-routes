// src/components/Logo.js
import React from 'react';
import styled from 'styled-components';
import logo from '../../assets/Logo.png';

const LogoImage = styled.img`
  position:absolute;
    left:50px;
    top:5px;
`;

const Logo = () => {
    return <LogoImage src={logo} alt="Logo" />;
};

export default Logo;
